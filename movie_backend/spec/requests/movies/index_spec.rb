# frozen_string_literal: true

RSpec.describe "GET /movies", type: :request do

  it "returns a list of movies" do
    get "/movies"

    expect(last_response).to be_successful
    expect(last_response.content_type).to eq("application/json; charset=utf-8")

    response_body = JSON.parse(last_response.body)

    expect(response_body).to eq({
      page: 1,
      total_results: 3,
      total_pages: 1,
      from_cache: false,
      results: [
        {
          poster_path: 'string or null',
          adult: false,
          overview: 'hello',
          release_date: '2022-12-12',
          genre_ids: [1,2,3],
          id: 1,
          original_title: "Hello World",
          original_language: "hu",
          title: "Hello world",
          backdrop_path: "/asd/s or null",
          popularity: 1,
          vote_count: 12,
          video: false,
          vote_average: 3
        },
        {
          poster_path: 'string or null',
          adult: false,
          overview: 'hello',
          release_date: '2022-12-12',
          genre_ids: [1,2,3],
          id: 1,
          original_title: "Hello World kjl",
          original_language: "hu",
          title: "Hello world",
          backdrop_path: "/asd/s or null",
          popularity: 1,
          vote_count: 12,
          video: false,
          vote_average: 3
        },
        {
          poster_path: 'string or null',
          adult: false,
          overview: 'helloasdasd',
          release_date: '2022-12-12',
          genre_ids: [1,2,3],
          id: 1,
          original_title: "Hello World asdasd",
          original_language: "hu",
          title: "Hello world",
          backdrop_path: "/asd/s or null",
          popularity: 1,
          vote_count: 12,
          video: false,
          vote_average: 3
        },
      ]
    })
  end
end
