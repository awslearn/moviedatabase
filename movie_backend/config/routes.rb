# frozen_string_literal: true

module MovieBackend
  class Routes < Hanami::Routes
    root { "Hello from Hanami" }
    get "/movies", to: "movies.index"
  end
end
