# frozen_string_literal: true

module MovieBackend
  class Settings < Hanami::Settings
    # Define your app settings here, for example:
    #
    # setting :my_flag, default: false, constructor: Types::Params::Bool
    setting :database_url, constructor: Types::String
    setting :movie_backend_api_url, constructor: Types::String
    setting :movie_backend_api_key, constructor: Types::String
  end
end
