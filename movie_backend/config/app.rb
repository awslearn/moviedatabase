# frozen_string_literal: true

require "hanami"

module MovieBackend
  class App < Hanami::App
  end
end
