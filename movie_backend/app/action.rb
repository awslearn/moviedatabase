# auto_register: false
# frozen_string_literal: true

require "hanami/action"

module MovieBackend
  class Action < Hanami::Action
  end
end
