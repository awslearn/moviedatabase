# frozen_string_literal: true

module MovieBackend
  module Actions
    module Movies
      class Index < MovieBackend::Action
        def handle(*, response)
          movies = [
            {title: "Test Driven Development"},
            {title: "Practical Object-Oriented Design in Ruby"}
          ]

          response.format = :json
          response.body = movies.to_json
        end
      end
    end
  end
end
